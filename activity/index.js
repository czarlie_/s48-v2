console.log(`Hello World 👋🌏`)
let posts = []
let count = 1

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
  // for preventing default behavior of the form (which is refreshing the page)
  e.preventDefault()

  posts.push({
    id: count,
    title: document.querySelector('#text-title').value,
    body: document.querySelector('#text-body').value
  })
  count++ // adds 1 to the current 'count' variable so that the i.d. will remain unique

  showPosts(posts)

  alert(`Successfully add a new post! 🥳`)
})

const showPosts = (posts) => {
  let postEntries = ''

  posts.forEach((post) => {
    // onclick="editPost"('${ post.id }')" => this should be the ultimate template! Otherwise result'll be error!
    postEntries += `
    <div id="post-${post.id}">
      <h3 id="post-title-${post.id}">${post.title}</h3>
      <p id="post-body-${post.id}">${post.body}</p>
      <button onclick="editPost('${post.id}')">Edit</button>
      <button onclick="deletePost('${post.id}')">Delete</button><br>
      <hr>
    </div>
    `
  })
  document.querySelector('#div-post-entries').innerHTML = postEntries
}

const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML
  let body = document.querySelector(`#post-body-${id}`).innerHTML

  document.querySelector('#text-edit-id').value = id
  document.querySelector('#text-title-edit').value = title
  document.querySelector('#text-body-edit').value = body
}

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
  e.preventDefault()

  document.querySelector(`#form-edit-post`)

  for (let i = 0; i <= posts.length; i++) {
    // checks if the post id of an existing post is equal to the value of the `#form-edit-id` field in the 'Edit' form
    // prettier-ignore
    if (posts[i].id.toString() === document.querySelector(`#text-edit-id`).value) {
      posts[i].title = document.querySelector('#text-title-edit').value
      posts[i].body = document.querySelector('#text-body-edit').value

      showPosts(posts)

      alert(`Succesfully updated the post! 🥳`)
      break
    }
  }
})

const deletePost = (id) => {
  posts = posts.filter((post) => {
    if (post.id.toString() !== id) return post
  })
  document.querySelector(`#post-${id}`).remove()
}
